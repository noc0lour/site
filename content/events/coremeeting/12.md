---
startdate:  2018-12-01
starttime: "13h"
linktitle: "Coremeeting 12"
title: "Coremeeting 12"
location: "HSBXL"
eventtype: "Meeting"
price: ""
series: "coremeeting"
---

Our monthly CoreMeeting. Discussing main issues like the bookkeeping.

# Agenda 
- the move 
- layout of the new space 
- what needs to be done before the move ?
_ what needs to be done ASAP after the move ?
- finances

